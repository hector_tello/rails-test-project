Rails.application.routes.draw do
  root "breeds#index"

  namespace :api do
    resources :breeds, only: [:show]
  end
end

class AddSubTypeToBreeds < ActiveRecord::Migration
  def change
    add_column :breeds, :sub_type, :string
  end
end

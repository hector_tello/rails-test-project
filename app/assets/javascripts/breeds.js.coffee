$(document).ready ->
  fetch_photo = (type, subType) ->
    url = "/api/breeds/#{type}?sub_type=#{subType}"

    $.ajax(url: url).done (data) ->
      photo = data.photo
      $('#image-container').html("<img src='#{photo.image}' />")


  init_default_select = () ->
    console.log('s')
    option = $('#bread-select')[0]
    selectOption = option.selectedOptions[0]
    type = option.value
    subType = selectOption.dataset.subType
    fetch_photo(type, subType)

  init_default_select()

  $('#bread-select').on 'change', (e) ->
    type = e.target.value
    selectOption = e.target.selectedOptions[0]
    subType = selectOption.dataset.subType
    fetch_photo(type, subType)

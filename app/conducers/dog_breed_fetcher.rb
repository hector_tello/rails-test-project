class DogBreedFetcher
  attr_reader :photos, :breads, :name, :sub_type

  def initialize(name=nil, sub_type = nil)
    @breads = []
    @photos = []
    @name   = name
    @sub_type = sub_type
  end

  def fetch
    image = fetch_info["message"]
    Photo.new(@name, image)
  end

  def self.fetch(name=nil, sub_type=nil)
    DogBreedFetcher.new(name, sub_type).fetch
  end

  def all
    breads = fetch_breads['message']
    @breads = breads.keys.map { |type| build_object(type, breads[type]) }.flatten
  end

  def self.all
    DogBreedFetcher.new().all
  end

  private

  def build_object(type, sub_types)
    return sub_types.map { |sub_type| init_breed(type, sub_type) } if sub_types.any?
    init_breed(type)
  end

  def init_breed(type, sub_type = '')
    Breed.new({ name: type, sub_type: sub_type })
  end

  def fetch_info
    url = "https://dog.ceo/api/breed/#{name}/images/random"
    url = "https://dog.ceo/api/breed/#{name}/#{sub_type}/images/random" if @sub_type.present?

    begin
      JSON.parse(RestClient.get(url).body)
    rescue Object => e
      default_body
    end
  end

  def default_body
    {
      "status"  => "success",
      "message" => "https://images.dog.ceo/breeds/cattledog-australian/IMG_2432.jpg"
    }
  end

  def fetch_breads
    begin
      JSON.parse(RestClient.get('https://dog.ceo/api/breeds/list/all').body)
    rescue Object => e
      {
        "status"  => "success",
        "message" => {}
      }
    end
  end
end

module Api
  class BreedsController < ApplicationController
    def show
      photo = DogBreedFetcher.fetch(params[:id], params[:sub_type])
      render json: { photo: photo }, status: :ok
    end
  end
end
